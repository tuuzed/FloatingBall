package com.tuuzed.floatingball

object FloatingBallConstant {
    /*
    <string name="_action_none_event" translatable="false">0</string>
    <string name="_action_back_event" translatable="false">1</string>
    <string name="_action_home_event" translatable="false">2</string>
    <string name="_action_task_event" translatable="false">3</string>
    <string name="_action_notification_event" translatable="false">4</string>
    <string name="_action_quick_event" translatable="false">5</string>
    <string name="_action_power_event" translatable="false">6</string>
    <string name="_action_split_screen_event" translatable="false">7</string>
     */
    const val ACTION_NONE_EVENT = "0"
    const val ACTION_BACK_EVENT = "1"
    const val ACTION_HOME_EVENT = "2"
    const val ACTION_TASK_EVENT = "3"
    const val ACTION_NOTIFICATION_EVENT = "4"
    const val ACTION_QUICK_EVENT = "5"
    const val ACTION_POWER_EVENT = "6"
    const val ACTION_SPLIT_SCREEN_EVENT = "7"
}
