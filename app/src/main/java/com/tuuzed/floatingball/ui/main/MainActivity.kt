package com.tuuzed.floatingball.ui.main

import android.app.Activity
import android.os.Bundle
import com.tuuzed.floatingball.util.replaceFragmentToActivity

class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        replaceFragmentToActivity(android.R.id.content, MainPreferenceFragment(), "MainPreferenceFragment")
    }

}
