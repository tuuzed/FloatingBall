package com.tuuzed.floatingball.ui.widget

import android.annotation.SuppressLint
import android.content.Context
import android.os.Vibrator
import android.view.MotionEvent
import android.view.View
import android.view.ViewConfiguration
import android.view.WindowManager
import android.widget.LinearLayout
import com.tuuzed.floatingball.R
import com.tuuzed.floatingball.util.DensityUtil
import kotlinx.android.synthetic.main.floating_ball.view.*

class FloatingBallView(context: Context) : LinearLayout(context) {
    private val mWindowManager: WindowManager = getContext().getSystemService(Context.WINDOW_SERVICE) as WindowManager
    private var mLayoutParams: WindowManager.LayoutParams? = null
    private var mBigBallX = 0f
    private var mBigBallY = 0f
    private val mBigBallOffset: Int = DensityUtil.dip2px(context, 10f)
    private var mOnGestureListener: OnGestureListener? = null
    private var mGestureDetectorHelper: GestureDetectorHelper

    init {
        View.inflate(context, R.layout.floating_ball, this)
        mGestureDetectorHelper = object : GestureDetectorHelper(this@FloatingBallView, context) {
            override fun onMove(event: MotionEvent?, distanceX: Float, distanceY: Float) {
                mLayoutParams?.apply {
                    // 计算偏移量并更新视图
                    x += distanceX.toInt()
                    y += distanceY.toInt()
                    mWindowManager.updateViewLayout(this@FloatingBallView, mLayoutParams)
                }
                mBigBallX = iv_big_ball.x
                mBigBallY = iv_big_ball.y
            }

            override fun onDown(event: MotionEvent?) {
                iv_ball.visibility = View.INVISIBLE
                iv_big_ball.visibility = View.VISIBLE
            }

            override fun onSingleClick(event: MotionEvent?) {
                //
            }

            override fun onSingleClickConfirmed(event: MotionEvent?) {
                mOnGestureListener?.onSingleClick()
            }

            override fun onDoubleClick(event: MotionEvent?) {
                mOnGestureListener?.onDoubleClick()

            }

            override fun onSlide(event: MotionEvent?, mode: Int) {
                when (mode) {
                    GestureDetectorHelper.MODE_RIGHT -> {
                        iv_big_ball.x = mBigBallX + mBigBallOffset
                        iv_big_ball.y = mBigBallY
                    }
                    GestureDetectorHelper.MODE_LEFT -> {
                        iv_big_ball.x = mBigBallX - mBigBallOffset
                        iv_big_ball.y = mBigBallY
                    }
                    GestureDetectorHelper.MODE_DOWN -> {
                        iv_big_ball.x = mBigBallX
                        iv_big_ball.y = mBigBallY + mBigBallOffset
                    }
                    GestureDetectorHelper.MODE_UP -> {
                        iv_big_ball.x = mBigBallX
                        iv_big_ball.y = mBigBallY - mBigBallOffset
                    }
                }
            }

            override fun onSlideConfirmed(event: MotionEvent?, mode: Int) {
                mOnGestureListener?.onSlide(mode)
                iv_big_ball.x = mBigBallX
                iv_big_ball.y = mBigBallY
            }

            override fun onFinish(event: MotionEvent?) {
                iv_ball.visibility = View.VISIBLE
                iv_big_ball.visibility = View.INVISIBLE
            }
        }
        iv_big_ball.post {
            mBigBallX = iv_big_ball.x
            mBigBallY = iv_big_ball.y
        }
    }

    fun setOnGestureListener(listener: OnGestureListener) {
        this.mOnGestureListener = listener
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        return mGestureDetectorHelper.onTouchEvent(event)
    }

    fun setLayoutParams(params: WindowManager.LayoutParams) {
        mLayoutParams = params
    }

    interface OnGestureListener {
        companion object {
            const val SLIDE_DOWN = GestureDetectorHelper.MODE_DOWN
            const val SLIDE_UP = GestureDetectorHelper.MODE_UP
            const val SLIDE_LEFT = GestureDetectorHelper.MODE_LEFT
            const val SLIDE_RIGHT = GestureDetectorHelper.MODE_RIGHT
        }

        fun onSingleClick()
        fun onDoubleClick()
        fun onSlide(slide: Int)
    }


    internal abstract class GestureDetectorHelper(private val view: View, context: Context) {
        private var lastDownTime = 0L
        private var lastDownX = 0f
        private var lastDownY = 0f
        // 标记是否长按(触摸)
        private var isLongTouch = false
        private var clickCount = 0
        // 标记是否触摸中
        private var isTouching = false
        // 最小滑动距离
        private var currentMode: Int = MODE_NONE
        private val vibrator: Vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        private var startX = 0f
        private var startY = 0f
        private val scaledTouchSlop = ViewConfiguration.get(context).scaledTouchSlop
        abstract fun onDown(event: MotionEvent?)
        abstract fun onSingleClick(event: MotionEvent?)
        abstract fun onSingleClickConfirmed(event: MotionEvent?)
        abstract fun onDoubleClick(event: MotionEvent?)
        abstract fun onMove(event: MotionEvent?, distanceX: Float, distanceY: Float)
        abstract fun onSlide(event: MotionEvent?, mode: Int)
        abstract fun onSlideConfirmed(event: MotionEvent?, mode: Int)
        abstract fun onFinish(event: MotionEvent?)

        companion object {
            private const val CLICK_LIMIT = 100L
            private const val LONG_CLICK_LIMIT = 300L
            private const val DOUBLE_CLICK_LIMIT = 150L
            const val MODE_DOWN = 0x000
            const val MODE_UP = 0x001
            const val MODE_LEFT = 0x002
            const val MODE_RIGHT = 0x003
            private const val MODE_MOVE = 0x004
            private const val MODE_NONE = 0x005

        }

        fun onTouchEvent(event: MotionEvent?): Boolean {
            if (event == null) return false
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    isTouching = true
                    onDown(event)
                    lastDownTime = System.currentTimeMillis()
                    lastDownX = event.x
                    lastDownY = event.y
                    view.postDelayed({
                        if (isTouching && currentMode == MODE_NONE && System.currentTimeMillis() - lastDownTime >= LONG_CLICK_LIMIT) {
                            isLongTouch = true
                            vibrator.vibrate(longArrayOf(0, 100), -1)
                            startX = event.rawX
                            startY = event.rawY
                        }
                    }, LONG_CLICK_LIMIT)
                }
                MotionEvent.ACTION_MOVE -> {
                    if (!isLongTouch) {
                        distinguishGesture(event)
                    } else if ((currentMode == MODE_NONE || currentMode == MODE_MOVE)) {
                        val rawX = event.rawX
                        val rawY = event.rawY
                        onMove(event, rawX - startX, rawY - startY)
                        startX = rawX
                        startY = rawY
                        lastDownX = event.x
                        lastDownY = event.y
                        currentMode = MODE_MOVE
                    }
                }
                MotionEvent.ACTION_CANCEL, MotionEvent.ACTION_UP -> {
                    isTouching = false
                    if (isLongTouch) {
                        isLongTouch = false
                    } else if (Math.abs(event.x - lastDownX) < scaledTouchSlop * 2
                            && Math.abs(event.y - lastDownY) < scaledTouchSlop * 2
                            && System.currentTimeMillis() - lastDownTime < CLICK_LIMIT) {
                        // 单击
                        if (clickCount != 2) {
                            clickCount++
                            onSingleClick(event)
                            if (clickCount == 1) {
                                view.postDelayed({
                                    if (clickCount == 2) onDoubleClick(event)
                                    else onSingleClickConfirmed(event)
                                    clickCount = 0
                                }, DOUBLE_CLICK_LIMIT)
                            }
                        }
                    } else {
                        onSlideConfirmed(event, currentMode)
                    }
                    onFinish(event)
                    currentMode = MODE_NONE
                }
            }

            return true
        }

        // 判断手势
        private fun distinguishGesture(event: MotionEvent) {
            val offsetX = event.x - lastDownX
            val offsetY = event.y - lastDownY
            if (Math.abs(offsetX) < scaledTouchSlop && Math.abs(offsetY) < scaledTouchSlop) {
                return
            }
            if (Math.abs(offsetX) > Math.abs(offsetY)) {
                if (offsetX > 0) {
                    if (currentMode == MODE_RIGHT) return
                    currentMode = MODE_RIGHT
                    onSlide(event, currentMode)
                } else {
                    if (currentMode == MODE_LEFT) return
                    currentMode = MODE_LEFT
                    onSlide(event, currentMode)
                }
            } else {
                if (offsetY > 0) {
                    if (currentMode == MODE_DOWN) return
                    currentMode = MODE_DOWN
                    onSlide(event, currentMode)
                } else {
                    if (currentMode == MODE_UP) return
                    currentMode = MODE_UP
                    onSlide(event, currentMode)
                }
            }
        }
    }
}