package com.tuuzed.floatingball.ui.main

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.preference.ListPreference
import android.preference.Preference
import android.preference.PreferenceFragment
import android.preference.SwitchPreference
import android.provider.Settings
import com.tuuzed.floatingball.BuildConfig
import com.tuuzed.floatingball.FloatingBallConstant
import com.tuuzed.floatingball.R
import com.tuuzed.floatingball.service.FloatingBallAccessibilityService
import com.tuuzed.floatingball.util.FloatingBallManager
import com.tuuzed.floatingball.util.LOGE
import com.tuuzed.floatingball.util.getAppSpItem
import com.tuuzed.floatingball.util.getBoolean

class MainPreferenceFragment : PreferenceFragment() {

    companion object {
        private const val TAG = "MainPreferenceFragment"
        fun getPrefFloatingBallValue(context: Context) = context.getAppSpItem("sw_pref_floating_ball", false)
        fun getPrefSingleClickValue(context: Context) = context.getAppSpItem("li_pref_single_click", FloatingBallConstant.ACTION_BACK_EVENT)
        fun getPrefDoubleClickValue(context: Context) = context.getAppSpItem("li_pref_double_click", FloatingBallConstant.ACTION_NONE_EVENT)
        fun getPrefSlideUpValue(context: Context) = context.getAppSpItem("li_pref_slide_up", FloatingBallConstant.ACTION_HOME_EVENT)
        fun getPrefSlideDownValue(context: Context) = context.getAppSpItem("li_pref_slide_down", FloatingBallConstant.ACTION_NOTIFICATION_EVENT)
        fun getPrefSlideRightValue(context: Context) = context.getAppSpItem("li_pref_slide_right", FloatingBallConstant.ACTION_TASK_EVENT)
        fun getPrefSlideLeftValue(context: Context) = context.getAppSpItem("li_pref_slide_left", FloatingBallConstant.ACTION_TASK_EVENT)

    }

    private lateinit var sw_pref_floating_ball: SwitchPreference
    private lateinit var li_pref_single_click: ListPreference
    private lateinit var li_pref_double_click: ListPreference
    private lateinit var li_pref_slide_up: ListPreference
    private lateinit var li_pref_slide_down: ListPreference
    private lateinit var li_pref_slide_right: ListPreference
    private lateinit var li_pref_slide_left: ListPreference
    private lateinit var pref_project_address: Preference
    private lateinit var pref_app_version: Preference
    private lateinit var pref_commit_date: Preference
    private lateinit var pref_commit_sha: Preference
    private var mDialog: Dialog? = null
    private val mMainHandler = Handler(Looper.getMainLooper())
    private val mListenAccessibilityServiceCallback = object : Runnable {
        override fun run() {
            if (FloatingBallAccessibilityService.isEnableAccessibilityService(activity)) {
                FloatingBallManager.showFloatingBall(activity)
                sw_pref_floating_ball.isChecked = true
            } else {
                sw_pref_floating_ball.isChecked = false
                mMainHandler.postDelayed(this, 100)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.main_pref_frag)
        findPreferences()
        initPreferences()
        showFloatingBall()
    }

    override fun onResume() {
        super.onResume()
        mMainHandler.removeCallbacks(mListenAccessibilityServiceCallback)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        mMainHandler.removeCallbacks(mListenAccessibilityServiceCallback)
    }

    private fun findPreferences() {
        sw_pref_floating_ball = findPreference("sw_pref_floating_ball") as SwitchPreference
        li_pref_single_click = findPreference("li_pref_single_click") as ListPreference
        li_pref_double_click = findPreference("li_pref_double_click") as ListPreference
        li_pref_slide_up = findPreference("li_pref_slide_up") as ListPreference
        li_pref_slide_down = findPreference("li_pref_slide_down") as ListPreference
        li_pref_slide_right = findPreference("li_pref_slide_right") as ListPreference
        li_pref_slide_left = findPreference("li_pref_slide_left") as ListPreference
        pref_project_address = findPreference("pref_project_address")
        pref_app_version = findPreference("pref_app_version")
        pref_commit_date = findPreference("pref_commit_date")
        pref_commit_sha = findPreference("pref_commit_sha")
    }

    private fun initPreferences() {
        if (sw_pref_floating_ball.getBoolean(false)) showFloatingBall() else hideFloatingBall()
        sw_pref_floating_ball.setOnPreferenceChangeListener { _, newValue ->
            LOGE(TAG, "newValue = $newValue")
            if ("true" == newValue.toString()) {
                showFloatingBall()
            } else {
                hideFloatingBall()
            }
            return@setOnPreferenceChangeListener true
        }
        initListPreference(li_pref_single_click, object : OnInitPreferenceCallback<ListPreference> {
            override fun call(preference: ListPreference, newValue: Any): Boolean {
                return true
            }
        })
        initListPreference(li_pref_double_click, object : OnInitPreferenceCallback<ListPreference> {
            override fun call(preference: ListPreference, newValue: Any): Boolean {
                return true
            }
        })

        initListPreference(li_pref_slide_up, object : OnInitPreferenceCallback<ListPreference> {
            override fun call(preference: ListPreference, newValue: Any): Boolean {
                return true
            }
        })
        initListPreference(li_pref_slide_down, object : OnInitPreferenceCallback<ListPreference> {
            override fun call(preference: ListPreference, newValue: Any): Boolean {
                return true
            }
        })
        initListPreference(li_pref_slide_right, object : OnInitPreferenceCallback<ListPreference> {
            override fun call(preference: ListPreference, newValue: Any): Boolean {
                return true
            }
        })
        initListPreference(li_pref_slide_left, object : OnInitPreferenceCallback<ListPreference> {
            override fun call(preference: ListPreference, newValue: Any): Boolean {
                return true
            }
        })
        pref_project_address.setOnPreferenceClickListener {
            val intent = Intent()
            intent.action = Intent.ACTION_VIEW
            intent.data = Uri.parse(it.summary.toString())
            activity.startActivity(intent)
            return@setOnPreferenceClickListener true
        }
        pref_app_version.summary = BuildConfig.VERSION_NAME
        pref_commit_date.summary = BuildConfig.COMMIT_DATE
        pref_commit_sha.summary = BuildConfig.COMMIT_SHA
    }

    private fun openAccessibilitySettingsUi() {
        val intent = Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)
        startActivity(intent)
        mMainHandler.postDelayed(mListenAccessibilityServiceCallback, 100)
    }

    private fun showFloatingBall() {
        if (FloatingBallAccessibilityService.isEnableAccessibilityService(activity)) {
            FloatingBallManager.showFloatingBall(activity)
            sw_pref_floating_ball.isChecked = true
            mMainHandler.removeCallbacks(mListenAccessibilityServiceCallback)
        } else {
            if (mDialog == null) {
                val dialog = AlertDialog.Builder(activity)
                        .setTitle(R.string.disable_accessibility_service)
                        .setMessage(R.string.disable_accessibility_service_message)
                        .setPositiveButton(R.string.enable, { dialog, _ ->
                            openAccessibilitySettingsUi()
                            dialog.dismiss()
                        })
                        .setNegativeButton(R.string.cancel, null)
                        .create()
                mDialog = dialog
                dialog.show()
            } else {
                mDialog?.apply { if (!this.isShowing) this.show() }
            }
            sw_pref_floating_ball.isChecked = false
        }
    }

    private fun hideFloatingBall() {
        FloatingBallManager.hideFloatingBall(activity)
    }

    private fun initListPreference(preference: ListPreference, callback: OnInitPreferenceCallback<ListPreference>) {
        preference.summary = preference.entry
        preference.setOnPreferenceChangeListener { _, newValue ->
            val index = preference.entryValues.indexOf(newValue.toString())
            if (index != -1) {
                preference.summary = preference.entries[index]
            }
            return@setOnPreferenceChangeListener callback.call(preference, newValue)
        }
    }

    interface OnInitPreferenceCallback<in T : Preference> {
        fun call(preference: T, newValue: Any): Boolean
    }

}
