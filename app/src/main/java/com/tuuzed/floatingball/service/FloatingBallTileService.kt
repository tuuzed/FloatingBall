package com.tuuzed.floatingball.service

import android.annotation.TargetApi
import android.os.Build
import android.service.quicksettings.Tile
import android.service.quicksettings.TileService
import com.tuuzed.floatingball.util.FloatingBallManager
import com.tuuzed.floatingball.util.toastShort

@TargetApi(Build.VERSION_CODES.N)
class FloatingBallTileService : TileService() {

    override fun onClick() {
        super.onClick()
        val tile = qsTile ?: return
        val state = tile.state
        when (state) {
            Tile.STATE_ACTIVE -> off(tile)
            Tile.STATE_INACTIVE -> on(tile)
            Tile.STATE_UNAVAILABLE -> {
            }
        }
    }

    private fun off(tile: Tile) {
        tile.state = Tile.STATE_INACTIVE
        FloatingBallManager.hideFloatingBall(this)
        tile.updateTile()
    }

    private fun on(tile: Tile) {
        if (FloatingBallAccessibilityService.isEnableAccessibilityService(this)) {
            tile.state = Tile.STATE_ACTIVE
            FloatingBallManager.showFloatingBall(this)
            tile.updateTile()
        } else {
            toastShort("Not enabled Floating Ball Service")
        }

    }

}