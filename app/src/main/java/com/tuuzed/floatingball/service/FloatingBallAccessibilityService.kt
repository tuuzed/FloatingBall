package com.tuuzed.floatingball.service

import android.accessibilityservice.AccessibilityService
import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.view.KeyEvent
import android.view.accessibility.AccessibilityEvent
import com.tuuzed.floatingball.util.AccessibilityServiceUtil

class FloatingBallAccessibilityService : AccessibilityService() {

    companion object {
        private var internalService: FloatingBallAccessibilityService? = null

        fun performGlobalAction(action: Int) = internalService?.performGlobalAction(action)

        fun onGesture(gestureId: Int) = internalService?.onGesture(gestureId)

        @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
        fun onKeyEvent(event: KeyEvent) = internalService?.onKeyEvent(event)

        fun isEnableAccessibilityService(context: Context) = AccessibilityServiceUtil.isAccessibilitySettingsOn(context, FloatingBallAccessibilityService::class.java)
    }

    override fun onCreate() {
        super.onCreate()
        internalService = this
    }

    override fun onDestroy() {
        super.onDestroy()
        internalService = null
    }

    override fun onAccessibilityEvent(event: AccessibilityEvent) {
    }

    override fun onInterrupt() {
    }
}