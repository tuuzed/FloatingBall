package com.tuuzed.floatingball.util

import android.accessibilityservice.AccessibilityService
import android.content.Context
import android.graphics.PixelFormat
import android.os.Build
import android.view.Gravity
import android.view.WindowManager
import com.tuuzed.floatingball.FloatingBallConstant
import com.tuuzed.floatingball.service.FloatingBallAccessibilityService
import com.tuuzed.floatingball.ui.main.MainPreferenceFragment
import com.tuuzed.floatingball.ui.widget.FloatingBallView

object FloatingBallManager {
    private var mFloatingBallView: FloatingBallView? = null
    private const val TAG = "FloatingBallManager"

    fun showFloatingBall(context: Context) {
        hideFloatingBall(context)
        val applicationContext = context.applicationContext
        if (mFloatingBallView == null) {
            val windowManager = getWindowManager(applicationContext)
            val params = WindowManager.LayoutParams()
            params.width = WindowManager.LayoutParams.WRAP_CONTENT
            params.height = WindowManager.LayoutParams.WRAP_CONTENT
            params.gravity = Gravity.CENTER
            params.type = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY else WindowManager.LayoutParams.TYPE_TOAST
            params.format = PixelFormat.RGBA_8888
            params.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL or WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
            val floatingBallView = FloatingBallView(applicationContext)
            floatingBallView.setLayoutParams(params)
            floatingBallView.setOnGestureListener(object : FloatingBallView.OnGestureListener {

                private fun performGlobalAction(value: String?) {
                    value?.apply {
                        when (this) {
                            FloatingBallConstant.ACTION_NONE_EVENT -> {
                            }
                            FloatingBallConstant.ACTION_BACK_EVENT -> FloatingBallAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK)
                            FloatingBallConstant.ACTION_HOME_EVENT -> FloatingBallAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME)
                            FloatingBallConstant.ACTION_TASK_EVENT -> FloatingBallAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_RECENTS)
                            FloatingBallConstant.ACTION_NOTIFICATION_EVENT -> FloatingBallAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_NOTIFICATIONS)
                            FloatingBallConstant.ACTION_QUICK_EVENT -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                FloatingBallAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_QUICK_SETTINGS)
                            }
                            FloatingBallConstant.ACTION_POWER_EVENT -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                FloatingBallAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_POWER_DIALOG)
                            }
                            FloatingBallConstant.ACTION_SPLIT_SCREEN_EVENT -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                FloatingBallAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_TOGGLE_SPLIT_SCREEN)
                            }
                        }
                    }
                }

                override fun onSingleClick() {
                    LOGD(TAG, "onSingleClick")
                    performGlobalAction(MainPreferenceFragment.getPrefSingleClickValue(applicationContext))
                }

                override fun onDoubleClick() {
                    LOGD(TAG, "onDoubleClick")
                    performGlobalAction(MainPreferenceFragment.getPrefDoubleClickValue(applicationContext))
                }

                override fun onSlide(slide: Int) {
                    when (slide) {
                        FloatingBallView.OnGestureListener.SLIDE_LEFT -> {
                            LOGD(TAG, "onSwipe: SLIDE_LEFT")
                            performGlobalAction(MainPreferenceFragment.getPrefSlideLeftValue(applicationContext))
                        }
                        FloatingBallView.OnGestureListener.SLIDE_RIGHT -> {
                            LOGD(TAG, "onSwipe: SLIDE_RIGHT")
                            performGlobalAction(MainPreferenceFragment.getPrefSlideRightValue(applicationContext))
                        }
                        FloatingBallView.OnGestureListener.SLIDE_UP -> {
                            LOGD(TAG, "onSwipe: SLIDE_UP")
                            performGlobalAction(MainPreferenceFragment.getPrefSlideUpValue(applicationContext))
                        }
                        FloatingBallView.OnGestureListener.SLIDE_DOWN -> {
                            LOGD(TAG, "onSwipe: SLIDE_DOWN")
                            performGlobalAction(MainPreferenceFragment.getPrefSlideDownValue(applicationContext))
                        }
                    }
                }
            })
            windowManager.addView(floatingBallView, params)
            mFloatingBallView = floatingBallView
        }
    }

    fun hideFloatingBall(context: Context) {
        val applicationContext = context.applicationContext
        if (mFloatingBallView != null) {
            val windowManager = getWindowManager(applicationContext)
            windowManager.removeView(mFloatingBallView)
            mFloatingBallView = null
        }
    }

    private var internalWindowManager: WindowManager? = null
    private fun getWindowManager(context: Context): WindowManager {
        if (internalWindowManager == null) {
            internalWindowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        }
        return internalWindowManager!!
    }
}
