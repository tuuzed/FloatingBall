package com.tuuzed.floatingball.util

import android.app.Activity
import android.app.Fragment
import android.content.Context
import android.preference.Preference
import android.support.annotation.IdRes
import android.support.annotation.IntDef
import android.util.Log
import android.widget.Toast
import com.tuuzed.floatingball.BuildConfig

// Activity

fun Activity.replaceFragmentToActivity(@IdRes containerViewId: Int, fragment: Fragment, tag: String = fragment::class.java.simpleName) = fragmentManager.beginTransaction().replace(containerViewId, fragment, tag).commit()

// Preference

fun Preference.getBoolean(defValue: Boolean) = sharedPreferences.getBoolean(key, defValue)
fun Preference.getFloat(defValue: Float) = sharedPreferences.getFloat(key, defValue)
fun Preference.getInt(defValue: Int) = sharedPreferences.getInt(key, defValue)
fun Preference.getLong(defValue: Long) = sharedPreferences.getLong(key, defValue)
fun Preference.getString(defValue: String) = sharedPreferences.getString(key, defValue)
fun Preference.getStringSet(defValue: Set<String>) = sharedPreferences.getStringSet(key, defValue)

private fun Context.getAppSp() = getSharedPreferences("${packageName}_preferences", Context.MODE_PRIVATE)
fun Context.getAppSpItem(key: String, defValue: Boolean) = getAppSp().getBoolean(key, defValue)
fun Context.getAppSpItem(key: String, defValue: Float) = getAppSp().getFloat(key, defValue)
fun Context.getAppSpItem(key: String, defValue: Int) = getAppSp().getInt(key, defValue)
fun Context.getAppSpItem(key: String, defValue: Long) = getAppSp().getLong(key, defValue)
fun Context.getAppSpItem(key: String, defValue: String) = getAppSp().getString(key, defValue)
fun Context.getAppSpItem(key: String, defValue: Set<String>) = getAppSp().getStringSet(key, defValue)

fun Context.setAppSpItem(key: String, value: Boolean) = getAppSp().edit().putBoolean(key, value).apply()
fun Context.setAppSpItem(key: String, value: Float) = getAppSp().edit().putFloat(key, value).apply()
fun Context.setAppSpItem(key: String, value: Int) = getAppSp().edit().putInt(key, value).apply()
fun Context.setAppSpItem(key: String, value: Long) = getAppSp().edit().putLong(key, value).apply()
fun Context.setAppSpItem(key: String, value: String) = getAppSp().edit().putString(key, value).apply()
fun Context.setAppSpItem(key: String, value: Set<String>) = getAppSp().edit().putStringSet(key, value).apply()

// Log

fun Any.LOGV(tag: String, msg: String) = if (BuildConfig.ENABLE_LOG) Log.v(tag, msg) else 0
fun Any.LOGV(tag: String, msg: String, tr: Throwable?) = if (BuildConfig.ENABLE_LOG) Log.v(tag, msg, tr) else 0
fun Any.LOGD(tag: String, msg: String) = if (BuildConfig.ENABLE_LOG) Log.d(tag, msg) else 0
fun Any.LOGD(tag: String, msg: String, tr: Throwable?) = if (BuildConfig.ENABLE_LOG) Log.d(tag, msg, tr) else 0
fun Any.LOGI(tag: String, msg: String) = if (BuildConfig.ENABLE_LOG) Log.i(tag, msg) else 0
fun Any.LOGI(tag: String, msg: String, tr: Throwable?) = if (BuildConfig.ENABLE_LOG) Log.i(tag, msg, tr) else 0
fun Any.LOGW(tag: String, msg: String) = if (BuildConfig.ENABLE_LOG) Log.w(tag, msg) else 0
fun Any.LOGW(tag: String, msg: String, tr: Throwable?) = if (BuildConfig.ENABLE_LOG) Log.w(tag, msg, tr) else 0
fun Any.LOGE(tag: String, msg: String) = if (BuildConfig.ENABLE_LOG) Log.e(tag, msg) else 0
fun Any.LOGE(tag: String, msg: String, tr: Throwable?) = if (BuildConfig.ENABLE_LOG) Log.e(tag, msg, tr) else 0

// Toast

private var sToast: Toast? = null
fun Context.toastShort(text: CharSequence) = toast(text, Toast.LENGTH_SHORT)
fun Context.toastLong(text: CharSequence) = toast(text, Toast.LENGTH_LONG)
@IntDef(flag = true, value = [Toast.LENGTH_SHORT, Toast.LENGTH_LONG])
annotation class Duration

fun Context.toast(text: CharSequence, @Duration duration: Int) {
    sToast?.cancel()
    sToast = Toast.makeText(this, text, duration)
    sToast?.show()
}