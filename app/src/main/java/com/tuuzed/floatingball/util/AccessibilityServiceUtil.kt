package com.tuuzed.floatingball.util

import android.content.Context
import android.provider.Settings
import android.text.TextUtils
import android.util.Log

object AccessibilityServiceUtil {
    private const val TAG = "AccessibilityUtils"

    fun <T> isAccessibilitySettingsOn(context: Context, clazz: Class<T>): Boolean {
        var accessibilityEnable = 0
        val serviceName = "${context.packageName}/${clazz.canonicalName}"
        try {
            accessibilityEnable = Settings.Secure.getInt(context.contentResolver, Settings.Secure.ACCESSIBILITY_ENABLED, 0)
        } catch (e: Exception) {
            Log.e(TAG, "get accessibility enable failed, the err: ${e.message}")
        }
        if (accessibilityEnable == 1) {
            val mStringColonSplitter = TextUtils.SimpleStringSplitter(':')
            val settingValue = Settings.Secure.getString(context.contentResolver,
                    Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES)
            if (settingValue != null) {
                mStringColonSplitter.setString(settingValue)
                while (mStringColonSplitter.hasNext()) {
                    val accessibilityService = mStringColonSplitter.next()
                    if (accessibilityService.toLowerCase() == serviceName.toLowerCase()) {
                        Log.v(TAG, "We've found the correct setting - accessibility is switched on!")
                        return true
                    }
                }
            }
        } else {
            Log.d(TAG, "Accessibility service disable")
        }
        return false
    }
}